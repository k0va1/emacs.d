;;; This fixed garbage collection, makes emacs start up faster ;;;;;;;
(setq gc-cons-threshold 402653184
      gc-cons-percentage 0.6)

(defvar startup/file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

(defun startup/revert-file-name-handler-alist ()
  (setq file-name-handler-alist startup/file-name-handler-alist))

(defun startup/reset-gc ()
  (setq gc-cons-threshold 16777216
	      gc-cons-percentage 0.1))

(add-hook 'emacs-startup-hook 'startup/revert-file-name-handler-alist)
(add-hook 'emacs-startup-hook 'startup/reset-gc)

;;;;;;;; This is all kinds of necessary
(require 'package)
(setq package-enable-at-startup nil)

(setq package-archives '(("ELPA"  . "http://tromey.com/elpa/")
                         ("gnu"   . "http://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org"   . "https://orgmode.org/elpa/")))
(package-initialize)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'use-package)         
  (package-refresh-contents    )      
  (package-install 'use-package))


(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (base16-eighties)))
 '(custom-safe-themes
   (quote
    ("9be1d34d961a40d94ef94d0d08a364c3d27201f3c98c9d38e36f10588469ea57" default)))
 '(global-whitespace-mode t)
 '(global-whitespace-newline-mode t)
 '(package-selected-packages
   (quote
    (robe enh-ruby-mode multiple-cursors evil-collection helm smartparens org-tempo yaml-mode projectile-rails ruby-end dumb-jump rbenv powerline-evil powerline powerline-default-theme treemacs-magit treemacs-icons-dired treemacs-projectile treemacs-evil treemacs flycheck company ag diff-hl evil-org evil-surround org-bullets evil which-key use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 140 :width normal :foundry "nil" :family "Menlo"))))
 '(whitespace-empty ((t (:background nil))))
 '(whitespace-hspace ((t (:background nil))))
 '(whitespace-indentation ((t (:background nil))))
 '(whitespace-line ((t (:background nil))))
 '(whitespace-newline ((t (:background nil))))
 '(whitespace-space ((t (:background nil))))
 '(whitespace-space-after-tab ((t (:background nil))))
 '(whitespace-space-before-tab ((t (:background nil))))
 '(whitespace-tab ((t (:background nil :foreground nil))))
 '(whitespace-trailing ((t (:background nil :foreground "#747369")))))
