* Emacs notes
** Hotkeys
*** Show documentation to the hotkey ~C-h k~
*** Show documentation to the function ~C-h f~
*** Show documentation to the variable ~C-h v~
*** Search documentation by regexp ~C-h a~
