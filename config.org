* Terminal
** Setting default shell to zsh
   #+BEGIN_SRC emacs-lisp

   #+END_SRC
** Shell hotkey
   #+BEGIN_SRC emacs-lisp
(global-set-key [f1] 'ansi-term)
   #+END_SRC
* Org
** Org Bullets
   #+BEGIN_SRC emacs-lisp
  (use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))
   #+END_SRC
** Org elipsis
	 #+BEGIN_SRC emacs-lisp
	 (setq org-ellipsis "⤵")
	 #+END_SRC
** Enable syntax highlighting
	 #+BEGIN_SRC emacs-lisp
	 (setq org-src-fontify-natively t)
	 #+END_SRC
** Use current window for editing code
	 #+BEGIN_SRC emacs-lisp
	 (setq org-src-window-setup 'current-window)
	 #+END_SRC
** Elisp block alias
	 #+BEGIN_SRC emacs-lisp
	 (eval-after-load 'org
	 '(progn
	 (add-to-list 'org-structure-template-alist
             '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC"))
		))
	 #+END_SRC
* Helm
	#+BEGIN_SRC emacs-lisp
	#+END_SRC
* Beacon
  #+BEGIN_SRC emacs-lisp
(use-package beacon
  :ensure t
  :config
    (beacon-mode 1))
  #+END_SRC
* Which-key
  #+BEGIN_SRC emacs-lisp
(use-package which-key
  :ensure t
  :config
  (which-key-mode))
  #+END_SRC
* Other settings
** Minimal UI
   #+BEGIN_SRC emacs-lisp
  (scroll-bar-mode -1)
  (tool-bar-mode   -1)
  (tooltip-mode    -1)
  (menu-bar-mode   -1)
  (setq inhibit-startup-message t)
  (setq ring-bell-function 'ignore)
  (set-window-scroll-bars (minibuffer-window) nil nil)
  (setq frame-title-format '((:eval (projectile-project-name))))
  (global-prettify-symbols-mode t)
   #+END_SRC
** Encoding
   #+BEGIN_SRC emacs-lisp
  (setq locale-coding-system 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (prefer-coding-system 'utf-8)
   #+END_SRC
** Highlight current line
   #+BEGIN_SRC emacs-lisp
(global-hl-line-mode)
   #+END_SRC
** Disable autosaves and backups
   #+BEGIN_SRC emacs-lisp
  (setq make-backup-files nil)
  (setq auto-save-default nil)
   #+END_SRC
** Yes/No to y/n
   #+BEGIN_SRC emacs-lisp
  (defalias 'yes-or-no-p 'y-or-n-p)
   #+END_SRC
** Improve scroll
   #+BEGIN_SRC emacs-lisp
  (setq scroll-conservatively 100)
   #+END_SRC
** Show parentheses
   #+BEGIN_SRC emacs-lisp
(show-paren-mode 1)
   #+END_SRC
** Add /n to all files while save
   #+BEGIN_SRC emacs-lisp
(setq require-final-newline t)
   #+END_SRC
** Don't allow two spaces after sentence end
   #+BEGIN_SRC emacs-lisp
(setq sentence-end-double-space nil)
   #+END_SRC
** Save and restore emacs sessions
   #+BEGIN_SRC emacs-lisp
(desktop-save-mode 1)
   #+END_SRC
** Tabs
   #+BEGIN_SRC emacs-lisp
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
   #+END_SRC
** Whitespaces
   I want to show whitespaces with bottom dot symbol without background hightlighting
   #+BEGIN_SRC emacs-lisp
   (progn
   ;; Make whitespace-mode with very basic background coloring for whitespaces.
   ;; http://ergoemacs.org/emacs/whitespace-mode.html
   (setq whitespace-display-mappings
   '((space-mark   ?\    [?\u0323]     [?.])	; space
   )))

(setq whitespace-line-column 120) ;; limit line length

(add-hook 'prog-mode-hook 'whitespace-mode)
   #+END_SRC
* Evil
** Enable evil mode
   #+BEGIN_SRC emacs-lisp
(use-package evil
  :ensure t
	:init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode))
   #+END_SRC
** Evil surround
   [[https://github.com/emacs-evil/evil-surround][Repo]]
   #+BEGIN_SRC emacs-lisp
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))
   #+END_SRC
** Evil org mode
   [[https://github.com/Somelauw/evil-org-mode][Repo]]
   #+BEGIN_SRC emacs-lisp
(use-package evil-org
  :ensure t
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))
   #+END_SRC
** Evil powerline
   #+BEGIN_SRC emacs-lisp
(use-package powerline-evil
:ensure t)
(powerline-evil-vim-color-theme)
   #+END_SRC
** Evil collection
   #+BEGIN_SRC emacs-lisp
(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))
  (define-key evil-insert-state-map (kbd "TAB") 'tab-to-tab-stop)
   #+END_SRC
* Follow window splits
  #+BEGIN_SRC emacs-lisp
(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)
  #+END_SRC
* Kill buffer without confirmation
  #+BEGIN_SRC emacs-lisp
(setq kill-buffer-query-functions (delq 'process-kill-buffer-query-function kill-buffer-query-functions))
  #+END_SRC
* Highlight uncommited changes
  #+BEGIN_SRC emacs-lisp
  (use-package diff-hl
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode))
  #+END_SRC
* Enable line numbers
  #+BEGIN_SRC emacs-lisp
(use-package linum-relative
  :ensure t
  :config
    (setq linum-relative-current-symbol "")
    (add-hook 'prog-mode-hook 'linum-relative-mode))
  #+END_SRC
* Rainbow mode
  #+BEGIN_SRC emacs-lisp
(use-package rainbow-mode
  :ensure t
  :init
    (add-hook 'prog-mode-hook 'rainbow-mode))
  #+END_SRC
* Fonts
  #+BEGIN_SRC emacs-lisp
  (setq hrs/default-font "Menlo")
(setq hrs/default-font-size 14)
(setq hrs/current-font-size hrs/default-font-size)

(setq hrs/font-change-increment 1.1)

(defun hrs/font-code ()
  "Return a string representing the current font (like \"Inconsolata-14\")."
  (concat hrs/default-font "-" (number-to-string hrs/current-font-size)))

(defun hrs/set-font-size ()
  "Set the font to `hrs/default-font' at `hrs/current-font-size'.
Set that for the current frame, and also make it the default for
other, future frames."
  (let ((font-code (hrs/font-code)))
    (add-to-list 'default-frame-alist (cons 'font font-code))
    (set-frame-font font-code)))

(defun hrs/reset-font-size ()
  "Change font size back to `hrs/default-font-size'."
  (interactive)
  (setq hrs/current-font-size hrs/default-font-size)
  (hrs/set-font-size))

(defun hrs/increase-font-size ()
  "Increase current font size by a factor of `hrs/font-change-increment'."
  (interactive)
  (setq hrs/current-font-size
        (ceiling (* hrs/current-font-size hrs/font-change-increment)))
  (hrs/set-font-size))

(defun hrs/decrease-font-size ()
  "Decrease current font size by a factor of `hrs/font-change-increment', down to a minimum size of 1."
  (interactive)
  (setq hrs/current-font-size
        (max 1
             (floor (/ hrs/current-font-size hrs/font-change-increment))))
  (hrs/set-font-size))

(define-key global-map (kbd "C-)") 'hrs/reset-font-size)
(define-key global-map (kbd "C-+") 'hrs/increase-font-size)
(define-key global-map (kbd "C-=") 'hrs/increase-font-size)
(define-key global-map (kbd "C-_") 'hrs/decrease-font-size)
(define-key global-map (kbd "C--") 'hrs/decrease-font-size)

(hrs/reset-font-size)
  #+END_SRC
* Themes
  #+BEGIN_SRC emacs-lisp
(use-package base16-theme
  :ensure t
  :config
  (load-theme 'base16-eighties t))
  #+END_SRC
* Silver searcher
  #+BEGIN_SRC emacs-lisp
(use-package ag
  :ensure t)
  #+END_SRC
* Company mode
  Autocomplete stuff
  #+BEGIN_SRC emacs-lisp
(use-package company
  :ensure t)
(add-hook 'after-init-hook 'global-company-mode)
(global-set-key (kbd "M-/") 'company-complete-common)
  #+END_SRC
* Dumb jump
  #+BEGIN_SRC emacs-lisp
  (use-package dumb-jump
  :ensure t
:config
(define-key evil-normal-state-map (kbd "M-.") 'dumb-jump-go)
)
  #+END_SRC
* Flycheck
  #+BEGIN_SRC emacs-lisp
  (use-package flycheck
  :ensure t)
  #+END_SRC
* Treemacs
  #+BEGIN_SRC emacs-lisp
(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if (executable-find "python") 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-display-in-side-window        t
          treemacs-file-event-delay              5000
          treemacs-file-follow-delay             0.2
          treemacs-follow-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-desc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-width                         35)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null (executable-find "python3"))))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("<f8>"      . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :after treemacs evil
  :ensure t)

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after treemacs magit
  :ensure t)
  #+END_SRC
** Change directories view
	 #+BEGIN_SRC emacs-lisp
	 (with-eval-after-load "treemacs"
  (setq treemacs-icon-open-png   (propertize "⊖ " 'face 'treemacs-directory-face)
        treemacs-icon-closed-png (propertize "⊕ " 'face 'treemacs-directory-face)))

	 #+END_SRC
* Powerline
  #+BEGIN_SRC emacs-lisp
(use-package powerline
:ensure t)
(powerline-center-evil-theme)

  #+END_SRC
* Ruby
** Rbenv
   #+BEGIN_SRC emacs-lisp
(use-package rbenv
:ensure t)
(global-rbenv-mode)
   #+END_SRC
** Ruby end
   #+BEGIN_SRC emacs-lisp
(use-package ruby-end
:ensure t)
   #+END_SRC
** Inf ruby
*** inf-ruby provides a REPL buffer connected to a Ruby subprocess.
	  #+BEGIN_SRC emacs-lisp
	(use-package inf-ruby
	:ensure t)
	  #+END_SRC
** Ruby refactor
** Enhanced ruby mode
   #+BEGIN_SRC emacs-lisp
 (use-package enh-ruby-mode
:ensure t
:config
)
   #+END_SRC
** Robe
#+BEGIN_SRC emacs-lisp
(use-package robe
:ensure t
:init
(add-hook 'ruby-mode-hook 'robe-mode)
)
#+END_SRC
* Projectile
** Rails
	 #+BEGIN_SRC emacs-lisp
	 (use-package projectile-rails
	 :ensure t
  :config
  (projectile-rails-global-mode))
	 #+END_SRC
* YAML
  #+BEGIN_SRC emacs-lisp
(use-package yaml-mode 
:ensure t)
  #+END_SRC
* Smartparens
	#+BEGIN_SRC emacs-lisp
	(use-package smartparens
	:ensure t
  :config
  (smartparens-global-mode t)
)
	#+END_SRC
* Multiple cursors
  #+BEGIN_SRC emacs-lisp
(use-package multiple-cursors
:ensure t
:config
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-unset-key (kbd "M-<down-mouse-1>"))
(global-set-key (kbd "M-<mouse-1>") 'mc/add-cursor-on-click)
)
  #+END_SRC
* Indent buffer
  #+BEGIN_SRC emacs-lisp
(defun indent-buffer ()
      (interactive)
      (save-excursion
        (indent-region (point-min) (point-max) nil)))
    (global-set-key [f12] 'indent-buffer)
  #+END_SRC

